from django.db import models


class User(models.Model):
    userid = models.CharField(max_length=50, primary_key=True, unique=True)
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'Users'
