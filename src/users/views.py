from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.response import Response

from src.users.models import User
from src.users.serializers import UserSerializer


class DisplayUser(generics.CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        user_id = request.data.get('userid', '')
        if not user_id:
            return Response(data={'error': 'User not Found'}, status=status.HTTP_404_NOT_FOUND)

        try:
            user = User.objects.get(pk=user_id)
            serializer = UserSerializer(user)
            return Response(data=serializer.data)
        except User.DoesNotExist:
            return Response(data={'error': 'User not Found'}, status=status.HTTP_404_NOT_FOUND)


class DisplayAllUser(generics.ListAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def list(self, request, *args, **kwargs):
        users = User.objects.all()
        serializers = UserSerializer(users, many=True)
        return Response(data=serializers.data)