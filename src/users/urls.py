from django.urls import path

from src.users.views import DisplayUser, DisplayAllUser

app_name='users'
urlpatterns = [
    path('DisplayUser', DisplayUser.as_view()),
    path('DisplayAllUser', DisplayAllUser.as_view())
]