from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from src.users.models import User


class UserSerializer(serializers.ModelSerializer):
    userid = serializers.CharField(max_length=50, validators=[UniqueValidator(queryset=User.objects.all())])
    name = serializers.CharField(max_length=50)

    class Meta:
        model = User
        fields = ['userid', 'name']