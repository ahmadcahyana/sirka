# SIRKA Test Interview
This Project is study case for test interview @*SIRKA*

## Quick start guide
### Requirements
For quick provisioning Please install docker at your environment.
- Python 3
- Docker (if you want)
- Postgresql

### Installation (Docker)
1. Clone the repo `git clone https://gitlab.com/ahmadcahyana/sirka.git`
2. Copy `.env.example` to `.env`
3. If you use docker just enter this command in project directory: `docker-compose up` and you can access the webapi trough `http://localhost:8000`

### Installation (Without Docker)
1. Create potgres database 
2. Clone the repo `git clone https://gitlab.com/ahmadcahyana/sirka.git`
3. Copy `.env.example` to `.env` and use your credential
4. install dependencies with this command `pip install -r requirements.txt`
5. migrate database `python manage.py migrate`
6. load initial data `python manage.py loaddata users`
7. run the project `python manage.py runserver`