#!/bin/bash -x
python manage.py migrate --noinput || exit 1
python manage.py loaddata users || exit 1
exec "$@"